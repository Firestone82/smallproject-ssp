# SSP Project - Smol

Authors: Mikula Pavel, Osoba Miroslav

## Installation

1. Clone this repository: `git clone https://gitlab.com/Firestone82/smallproject-ssp.git`
2. Install required packages - `pip install -r requirements.txt`
3. Build pages - `mkdocs build --site-dir public`
